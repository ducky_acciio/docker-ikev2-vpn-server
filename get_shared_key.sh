#!/usr/bin/env bash

docker exec ikev2-vpn-server cat /etc/ipsec.secrets | sed 's/.*"\(.*\)"/\1/g'
